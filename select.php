<?php
/**
 * Script produisant un fichier texte contenant les adresses mails des personnels de l'UMR G-EAU selon certains filtres
 */

define('IN_SCRIPT', TRUE);

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */

// MAJ users avec les actions automatiques de Community Builder
file('https://g-eau.fr/index.php/fr/?option=com_comprofiler&view=pluginclass&plugin=cbautoactions&action=general&token=dffcb7ff50101eb5f203f6d3c413b128&Itemid=282&format=raw');

// Connexion à la base de données (renvoie $conn le mySQLi object)
include 'connect.php';

$db_field = [
    "institute" => "cb_etablissement",
    "grade"     => "cb_grade",
    "site"      => "cb_site",
    "axe"       => "cb_axe",
    "categorie" => "cb_categorie"
];

// Requête de sélection de tous les agents actuellement présents dans l'UMR
$sql_select = 'SELECT email FROM `j2017_users` INNER JOIN `j2017_comprofiler` AS ue USING(id)
    WHERE (email != "" AND CURRENT_DATE()>=cb_datedbut AND (CURRENT_DATE()<=cb_datefin OR cb_pasdedatedesortie="oui"))';

// Définition des filtres des listes
$where = array();
$where['tous'] = 'ue.cb_categorie != "NC"';
$where['interne'] = 'ue.cb_categorie != "Chercheurs Associes"';
$where['lavalette'] = $where['tous'].' AND '.$where['interne'].' AND ue.cb_site = "Hydropolis Lavalette"';
$where['montpellier'] = $where['tous'].' AND '.$where['interne'].' AND ue.cb_site IN ("Montpellier", "Hydropolis Lavalette")';
$where['permanents'] = $where['tous'].' AND '.$where['interne'].' AND ue.cb_categorie NOT IN ("Doc","Temp ATER-PD")';
$where['doctorants'] = '(ue.cb_categorie="Doc" OR (ue.cb_categorie="Temp ATER-PD" AND ue.cb_grade="POST-DOC"))';
$where['temporaires'] = $where['tous'].' AND '.$where['interne'].' AND ue.cb_categorie IN ("Doc","Temp ATER-PD")';

function getHelp() {
    global $db_field;
    $msg = "**** HELP ****</br>".
        "URL should be formated as followed:</br>".
        "Minimum format: https://g-eau.fr/z_geau/select2.php?list=[LIST]</br>".
        "with [LIST] in: ".implode(', ', allowedFieldValues('list'))."</br></br>".
        "Optional fields are allowed with the format:</br>".
        "https://g-eau.fr/z_geau/select2.php?list=[LIST]&fieldA=[valueA1,valueA2,...]&fieldB=[valueB1, valueB2,...]&...<br><br>";
    foreach(array_keys($db_field) as $get_field){
        $msg .= "Field: {$get_field}, with possible values: ".implode(', ', allowedFieldValues($get_field))."</br>";
    }
    return $msg;
}

function howToDieProperly($error_msg) {
    global $db_field;
    $msg = "**** ERROR ****</br>".$error_msg."</br></br>".getHelp();
    die($msg);
}

// Sélection de la liste, écriture de la requête
if(!isset($_GET['list'])) {
    die(getHelp());
}
if(!isset($where[$_GET['list']])) {
    howToDieProperly("error : list not supported : " . $_GET['list']);
}

$query = $sql_select . ' AND ' . $where[$_GET['list']];

function sql_query_fetch($query) {
    //echo($query."\n");
    global $mysqli;
    if (!$result = $mysqli->query($query)) {
        die('error query failed: '.$query);
    }
    return $result->fetch_all(MYSQLI_NUM);
}

function allowedFieldValues($get_field) {
    global $mysqli, $db_field;
    if ($get_field == 'list') {
        return ['tous', 'interne', 'lavalette', 'montpellier', 'permanents', 'doctorants', 'temporaires'];
    } else {
        $sql = "SELECT {$db_field[$get_field]} FROM `j2017_comprofiler`
            WHERE (CURRENT_DATE()>=cb_datedbut AND (CURRENT_DATE()<=cb_datefin OR cb_pasdedatedesortie=\"oui\"))
            GROUP BY {$db_field[$get_field]}; ";
        return array_column(sql_query_fetch($sql), 0);
    }
}

function filterField($get_field) {
    global $mysqli, $db_field;
    if(isset($_GET[$get_field])) {
        $values = explode(",",$_GET[$get_field]);
        $allowedValues = allowedFieldValues($get_field);
        foreach($values as $value) {
            if(!in_array($value, $allowedValues)) {
                howToDieProperly("error : value \"{$value}\" is not available for field \"{$get_field}\"");
            }
        }
        return " AND ue.{$db_field[$get_field]} IN (\"".implode("\", \"", $values)."\")";
    }
    return '';
}

foreach(array_keys($db_field) as $get_field){
    $query .= filterField($get_field);
}

$query .= ' ORDER BY email;';

// Exécution de la requête SQL
$emails = sql_query_fetch($query);

// Création du fichier texte
header("Content-Type: text/plain");
foreach($emails as $email) {
    echo($email[0]."\n");
}

$mysqli->close();
?>