<?php
/**
 * Paramètres de connexion à la base de données MySQL
 */

// Protection pour éviter une exécution de ce script depuis l'extérieur
if(! defined('IN_SCRIPT')) {
    die('error: hacking attempt');
}

/* // Connexion test locale
 $servername = 'localhost';
 $username = 'username';
 $password = 'password';
 $dbname = 'test'; */

 // Connexion serveur g-eau.fr
 $servername = 'xxxxxxxxxx.mysql.db';
 $username = 'xxxxxxxxxxxx';
 $password = 'xxxxxxxxxxxx';
 $dbname = 'xxxxxxxxxxxx';

 // Create connection
 $mysqli = new mysqli($servername, $username, $password, $dbname);

 // Check connection
 if ($mysqli->connect_error) {
     die('Connection failed: ' . $mysqli->connect_error);
 }
?>