# Script de production de listes d'emails pour les listes de diffusion de l'UMR G-EAU

## Description

L'UMR G-EAU utilise des listes de diffusion hébergées sur le serveur sympa ex-Irstea (https://sympa.irstea.fr). Ces listes sont alimentées et automatiquement mises à jour à partir d'une base de données des personnels de l'UMR hébergées sur le site internet de l'UMR (https://g-eau.fr).

Le présent script, installé sur le serveur de l'UMR G-EAU est chargé de transmettre
au serveur sympa d'Irstea les listes d'emails de chacune des listes de diffusion de l'UMR.
Il permet aussi d'effectuer des requêtes plus ciblées pour récupérer des listes de mails.

De nombreuses autres requêtes sont en théorie possible, contactez david.dorchies@inrae.fr
pour suggérer de nouveaux filtres de requête.

## Listes de diffusions de l'UMR

| Adresse @list.g-eau.fr | Description                                          |
| ---------------------- | ---------------------------------------------------- |
| tous                   | les personnels actuellement présents dans l'UMR y compris les chercheurs associés et excepté la direction des tutelles |
| interne                | les personnels actuellement présents dans l'UMR excepté les chercheurs associés et y compris la direction des tutelles |
| lavalette              | les personnels actuellement présents dans l'UMR présents sur le site de Lavalette     |
| montpellier            | les personnels actuellement présents dans l'UMR sur les sites de Montpellier excepté les chercheurs associés et la direction des tutelles |
| permanents             | les permanents actuellement présents dans l'UMR excepté les chercheurs associés et la direction des tutelles |
| doctorants             | les doctorants et post-doctorants actuellement présents dans l'UMR |

## Comment effectuer une requête ciblée

### Pré-requis

La requête s'effectue en tapant une URL dans un navigateur internet (Chrome, Edge, Firefox...)
sur un ordinateur connecté au réseau Inrae (soit sur site, soit via VPN).

### Syntaxe des requêtes

L'URL se compose ainsi:

```
 https://g-eau.fr/z_geau/select.php?list=[LIST]&champ1=[val1,val2,...]&champ2=[val1,val2,...]&...
```

La requête est basée sur une des listes décrites ci-dessus via le paramètre `list`
qui peut ensuite être filtrée via différents champs cumulés (axe, grade, catégorie...) et pour chaque champ filtrant
plusieurs valeurs peuvent être définies séparées par des virgules (sans espaces).

Aller directement sur https://g-eau.fr/z_geau/select.php pour afficher la page d'aide
contenant les valeurs possibles pour les champs et les valeurs des champs.

### Exemples de requêtes

Pour obtenir la liste des emails :
 - des personnels temporaires de la tutelle Inrae:  https://g-eau.fr/z_geau/select.php?list=temporaires&institute=INRAE
 - des personnels présents à Lavalette: https://g-eau.fr/z_geau/select.php?list=lavalette
 - des stagiaires INRAE présents dans l'UMR: https://g-eau.fr/z_geau/select.php?list=tous&grade=Stagiaire&institute=INRAE
 - des ITA présents à Lavalette: https://g-eau.fr/z_geau/select.php?list=lavalette&categorie=A%20ITA-IATOS,ITA-IATOS-EPIC

 ### Bugs connus

 Le champ `grade` contient des modalités qui contiennent des accents pour lesquelles il n'est pas possible d'avoir de filtre.

## Sécurité

L'accès au répertoire d'installation du script sur le serveur de l'UMR G-EAU est
limité aux IP du réseau interne Inrae par un fichier `.htaccess`.
